const { Router } = require('express');
const dbConnection = require('../../db.conection');
const registerUserActivity = require('../routes-user-activity/routes.user.activity');

const router = Router();

router.get("/buscar/cursos/:admonGral/:search", async (req, res) => {

    try {
        const admonGral = req.params.admonGral;  
        const search = req.params.search;
        
        const querySearchCourses = `
        SELECT C.* FROM CURSOS C JOIN CURSOS_REGISTRADOS CR ON C.IDCURSO = CR.IDCURSO WHERE CR.IDADMON = $1 AND LOWER(C.TITULO) LIKE LOWER($2) AND C.FECHA > CURRENT_DATE + INTERVAL '10 hours'`;

        const queryParams = [admonGral, `%${search}%`];

        await dbConnection.query("BEGIN");
        const response = await dbConnection.query(querySearchCourses, queryParams);
        await dbConnection.query("COMMIT");
        await registerUserActivity('alvaro.espindola.torres@gmail.com',6,search);

        res.status(200).json(response.rows);
    } catch (error) {
        console.log(error);
        return res.status(500).json({ error: 'Error en el servidor' });
        
    }
});



module.exports = router;