const { Router } = require('express');
const dbConnection = require('../../db.conection');
const router = Router();


router.post("/signup/register", async (req, res) => {
    try {
        const userData = req.body.userData;
        const imgDefault = "https://unavatar.io/default@gmail.com";
        
        const insertUserQuery = `
            INSERT INTO USUARIOS (CORREO, RFCCORTO, NUMEMPLEADO, NOMBRE, APEPAT, APEMAT, ADMONGRAL, ADMONCTRAL, FECHAREG, URLIMGUSUARIO)
            VALUES ($1, $2, $3, $4, $5, $6, $7, $8, CURRENT_TIMESTAMP - interval '6 hour', $9)
        `;
        const userValues = [
            userData.correo, userData.rfccorto, userData.numempleado,
            userData.nombre, userData.apepat, userData.apemat,
            userData.admongral, userData.admoncentr, imgDefault
        ];

        const roleQuery = 'INSERT INTO ASIGNACION_ROLES (CORREO, IDROL) VALUES ($1, $2)';
        const roleValues = [userData.correo, '3'];

        // Ejecuta ambas consultas de forma transaccional
        await dbConnection.query('BEGIN');
        await dbConnection.query(insertUserQuery, userValues);
        await dbConnection.query(roleQuery, roleValues);
        await dbConnection.query('COMMIT');

        res.status(200).json({ message: 'Registro exitoso' });
    } catch (err){
        // En caso de error, realiza un rollback y maneja la excepción
        await dbConnection.query('ROLLBACK');
        console.error(err);
        res.status(500).json({ error: 'Error en el servidor' });
    }
});

router.post("/signup/validation/userRegistered", async (req, res) => {
    try {
        const { email } = req.body;

        const query = 'SELECT * FROM USUARIOS WHERE CORREO = $1';
        const response = await dbConnection.query(query, [email]);
        
        const userExists = response.rows.length >= 1;
        return res.json({ exists: userExists });
    } catch (error) {
        return res.status(500).json({ error: 'Error en el servidor' });
    }
});

module.exports = router;