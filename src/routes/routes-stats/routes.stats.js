const { Router } = require('express');
const dbConnection = require('../../db.conection');
const router = Router();

router.get("/stats/Pendientes/:userEmail", async(req,res) => {
    const queryPendingCourses = `
    SELECT COUNT(*)
    FROM SUSCRIPCIONES S
    JOIN CURSOS C ON S.IDCURSO = C.IDCURSO
    WHERE S.CORREO = ($1)
    AND C.FECHA > CURRENT_TIMESTAMP - interval '6 hour';`;

    try {
        const userEmail = req.params.userEmail;
        const countPendings = await dbConnection.query(queryPendingCourses, [userEmail]);
        res.status(200).json(countPendings.rows[0].count);

    } catch (error) {
        res.status(500).json('Internal server error');
    }
});

router.get("/stats/Terminados/:userEmail", async(req,res) => {
    const queryFinishedCourses = `
    SELECT COUNT(*)
    FROM SUSCRIPCIONES S
    JOIN CURSOS C ON S.IDCURSO = C.IDCURSO
    WHERE S.CORREO = ($1)
    AND CURRENT_TIMESTAMP - interval '6 hour' > C.FECHA;`;

    try {
        const userEmail = req.params.userEmail;
        const countFinished = await dbConnection.query(queryFinishedCourses, [userEmail]);
        res.status(200).json(countFinished.rows[0].count);

    } catch (error) {
        res.status(500).json('Internal server error');
    }
});

router.get("/stats/Favoritos/:userEmail", async(req,res) => {
    const queryFavoritesCourses = `
    SELECT COUNT(CURSOS_FAVORITOS.IDCURSO) AS Cantidad_Cursos_Favoritos
    FROM CURSOS_FAVORITOS
    JOIN CURSOS ON CURSOS_FAVORITOS.IDCURSO = CURSOS.IDCURSO
    WHERE CURSOS_FAVORITOS.CORREO = ($1)
    AND CURSOS.FECHA >= CURRENT_DATE - interval '6 hour';`;

    try {
        const userEmail = req.params.userEmail;
        const countFavorites = await dbConnection.query(queryFavoritesCourses, [userEmail]);
        res.status(200).json(countFavorites.rows[0].count);

    } catch (error) {
        res.status(500).json('Internal server error');
    }
});

module.exports = router;