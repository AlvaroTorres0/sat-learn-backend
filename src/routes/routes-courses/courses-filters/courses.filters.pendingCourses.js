const { Router } = require('express');
const dbConnection = require('../../../db.conection');

const router = Router();

router.get('/cursos/pendientes/:userEmail', async (req,res) => {
    const queryGetCursosPendientes = `
        SELECT CURSOS.IDCURSO,CURSOS.TITULO,CURSOS.FECHA,CURSOS.IMGCURSO
        FROM CURSOS
        INNER JOIN SUSCRIPCIONES ON CURSOS.IDCURSO = SUSCRIPCIONES.IDCURSO
        WHERE SUSCRIPCIONES.CORREO = ($1)
        AND CURSOS.FECHA >= CURRENT_TIMESTAMP - interval '10 hour'
        AND CURSOS.FECHA <= CURRENT_TIMESTAMP + interval '2 day' - interval '10 hour'`;

    try {
        const userEmail = [req.params.userEmail];
        await dbConnection.query('BEGIN');
        const pendingCourses = await dbConnection.query(queryGetCursosPendientes,userEmail);
        await dbConnection.query('COMMIT');
        console.log(pendingCourses);
        res.status(200).json(pendingCourses.rows);
    }catch (err) {
        await dbConnection.query('ROLLBACK');
        res.status(500).json({ message: 'Error interno del servidor' });
    }
});

module.exports = router