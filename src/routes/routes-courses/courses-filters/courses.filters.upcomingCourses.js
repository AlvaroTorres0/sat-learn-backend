const { Router } = require('express');
const dbConnection = require('../../../db.conection');

const router = Router();

router.get('/cursos/proximamente/:admonUser', async (req,res) => {
    const queryGetCursosProximamente = `
        SELECT *
        FROM CURSOS C
        INNER JOIN CURSOS_REGISTRADOS CR ON C.IDCURSO = CR.IDCURSO
        WHERE C.FECHA IS null
        AND CR.IDADMON = ($1)`;
    
    try {
        const admonUser = [req.params.admonUser];
        await dbConnection.query('BEGIN');
        const coursesUpComing = await dbConnection.query(queryGetCursosProximamente,admonUser);
        await dbConnection.query('COMMIT');
        res.status(200).json(coursesUpComing.rows);
    }catch (err) {
        await dbConnection.query('ROLLBACK');
        res.status(500).json({ message: 'Error interno del servidor' });
    }
});

module.exports = router