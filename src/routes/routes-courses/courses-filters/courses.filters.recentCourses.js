const { Router } = require('express');
const dbConnection = require('../../../db.conection');

const router = Router();

router.get("/cursos/recientes/:admonUser", async(req,res) => {
    const query = `
        SELECT C.*
        FROM CURSOS C
        INNER JOIN CURSOS_REGISTRADOS CR ON C.IDCURSO = CR.IDCURSO
        INNER JOIN ADMONSGRALES AG ON CR.IDADMON = ($1)
        WHERE AG.IDADMONGRAL = ($1)
        AND C.FECHAREGISTRO >= CURRENT_TIMESTAMP - INTERVAL '1 days 6 hours' 
        ORDER BY C.FECHA DESC;
    `;
        
    try {
        const admonUser = [req.params.admonUser];        
        await dbConnection.query('BEGIN');
        const response = await dbConnection.query(query,admonUser);
        await dbConnection.query('COMMIT');
        res.status(200).json(response.rows);

    } catch (error) {
        await dbConnection.query('ROLLBACK');
        res.status(500).json({ error: 'Error en el servidor' });
    }
});

module.exports = router;