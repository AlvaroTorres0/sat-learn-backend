const { Router } = require('express');
const dbConnection = require('../../../db.conection');

const router = Router();

router.get("/cursos/recomendados/:userEmail/:idAdmon", async(req,res) => {
    const queryGetRecommendedCourses = `
    SELECT DISTINCT c.*
    FROM CURSOS c
    JOIN (
        SELECT IDACTIVIDAD, INFORMACIONEXTRA, CORREO
        FROM REGISTRO_ACTIVIDAD
        WHERE CORREO = ($2) -- Reemplaza con el correo del usuario específico
        AND IDACTIVIDAD = 6 -- Actividades de búsqueda
        ORDER BY FECHA DESC
        LIMIT 3 -- Obtener las tres actividades más recientes
    ) AS ra_recent ON c.TITULO ILIKE '%' || ra_recent.INFORMACIONEXTRA || '%'
    JOIN CURSOS_REGISTRADOS cr ON c.IDCURSO = cr.IDCURSO
    WHERE cr.IDADMON = ($1)
    AND c.FECHA > CURRENT_DATE + INTERVAL '10 hours'`;

    try {
        const userEmail = req.params.userEmail;
        const idAdmon = req.params.idAdmon;
        await dbConnection.query('BEGIN');
        
        const response = await dbConnection.query(queryGetRecommendedCourses,[idAdmon,userEmail]);
        await dbConnection.query('COMMIT');
        res.status(200).json(response.rows);

    } catch (error) {
        await dbConnection.query('ROLLBACK');
        res.status(500).json({ error: 'Error en el servidor' });
    }
});

module.exports = router;