const { Router } = require('express');
const dbConnection = require('../../../db.conection');

const router = Router();

router.get("/cursos/populares/:admonUser", async (req, res) => {
    const queryCursosRecomendados = `
    SELECT c.IDCURSO, c.INSTRUCTOR, c.FECHA, c.DESCRIPCION, c.IMGCURSO, c.TITULO, c.CAPACIDAD, COUNT(s.IDCURSO) AS SUSCRIPCIONES,
    (COUNT(s.IDCURSO) * 100.0 / c.CAPACIDAD) AS PORCENTAJE_OCUPADO
    FROM CURSOS c
    LEFT JOIN SUSCRIPCIONES s ON c.IDCURSO = s.IDCURSO
    INNER JOIN CURSOS_REGISTRADOS cr ON c.IDCURSO = cr.IDCURSO
    WHERE cr.IDADMON = ($1)
    GROUP BY c.IDCURSO, c.TITULO, c.CAPACIDAD
    HAVING (COUNT(s.IDCURSO) * 100.0 / c.CAPACIDAD) >= 50;

        
        `;
        
    try {
        const admonUser = [req.params.admonUser];        
        await dbConnection.query('BEGIN');
        const response = await dbConnection.query(queryCursosRecomendados,admonUser);
        await dbConnection.query('COMMIT');
        res.status(200).json(response.rows);
    } catch (error) {
        await dbConnection.query('ROLLBACK');
        res.status(500).json({ error: 'Error en el servidor' });
    }

});

module.exports = router;