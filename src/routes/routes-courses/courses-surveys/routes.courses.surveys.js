const { Router } = require("express");
const dbConnection = require("../../../db.conection");

const router = Router();

router.post("/cursos/encuestas", async (req,res) => {
    const querySurveyRegister = 'INSERT INTO RESPUESTAS_ENCUESTAS (IDCURSO,IDPREGUNTA,CORREO,PUNTUACION) VALUES ($1,$2,$3,$4)';

    try{
        const courseID = req.body.courseID;
        const userEmail = req.body.userEmail;
        const surveyResponses = req.body.surveyResponses;
    
        await dbConnection.query('BEGIN');
        for (let index = 0; index < surveyResponses.length; index++) {
            const dataQuery = [courseID,index + 1,userEmail,Number(surveyResponses[index])];
            await dbConnection.query(querySurveyRegister,dataQuery);
        }
        await dbConnection.query('COMMIT');
        res.status(200).json({ message: 'Registro exitoso' });

    } catch(err){
        await dbConnection.query('ROLLBACK');
        res.status(500).json({ message: 'Error interno del servidor' });
    }
});

router.get("/cursos/encuestas/validatesurvey/:courseID/:userEmail", async (req,res) => {
    const queryValidateSurvey = 'SELECT COUNT(*) AS total_respuestas FROM respuestas_encuestas WHERE correo = $1 AND idcurso = $2';

    const courseID = req.params.courseID;
    const userEmail = req.params.userEmail;

    try {
        const { rows } = await dbConnection.query(queryValidateSurvey, [userEmail, courseID]);
        const totalRespuestas = rows[0].total_respuestas; // Obtener el valor del conteo de la primera fila

        res.status(200).json({ total_respuestas: totalRespuestas });
        
    } catch (error) {
        
    }
});

module.exports = router;