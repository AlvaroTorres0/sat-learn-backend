const { Router } = require('express');
const router = Router();
const dbConnection = require('../../db.conection');

router.post("/suscripcion/curso", async(req,res) => {
    const querySubscripcion = `INSERT INTO SUSCRIPCIONES (CORREO,IDCURSO,FECHAINSCRIP) VALUES ($1,$2,CURRENT_TIMESTAMP - interval '6 hour')`;
    try{
        const dataSubscripcion = req.body.dataSubscripcion;
        const dataSubscripcionQuey = [dataSubscripcion.correoUsuario, dataSubscripcion.idCurso];
        
        await dbConnection.query("BEGIN");
        await dbConnection.query(querySubscripcion,dataSubscripcionQuey);
        await dbConnection.query("COMMIT")
        res.status(200).json({ message: 'Registro exitoso' });
        
    }catch(err){
        res.status(500).json({ message: 'Error en el servidor' });
        await dbConnection.query("ROLLBACK");
    }
});

router.get("/validar/suscripcion/:userEmail/:courseId", async(req,res) => {
    const queryValidateSuscription = "SELECT * FROM SUSCRIPCIONES WHERE CORREO = $1 AND IDCURSO = $2";
    try {
        const userEmail = req.params.userEmail;
        const courseId = req.params.courseId;

        const dataValidateSuscription = [userEmail, courseId];
        await dbConnection.query("BEGIN");
        const responseValidateSuscription = await dbConnection.query(queryValidateSuscription, dataValidateSuscription);
        await dbConnection.query("COMMIT");
        if(responseValidateSuscription.rows.length > 0){
            res.send(true);
        }else{
            res.send(false);
        }
    } catch (error) {
        await dbConnection.query("ROLLBACK");
        res.status(500).json({ message: 'Error en el servidor' });
    }
});

module.exports = router;