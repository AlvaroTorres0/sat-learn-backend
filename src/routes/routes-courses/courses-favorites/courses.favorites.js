const { Router } = require('express');
const dbConnection = require('../../../db.conection');

const router = Router();


router.post("/cursos/favoritos/nuevo", async(req,res) => {
    try {
        const queryPostNewFavoriteCourse = `INSERT INTO CURSOS_FAVORITOS (correo, idcurso) VALUES ($1, $2)`;
        const { userEmail, idCourse } = req.body.dataFavoriteCourse;
        const dataQuery = [userEmail, idCourse];

        await dbConnection.query('BEGIN');
        await dbConnection.query(queryPostNewFavoriteCourse, dataQuery);
        await dbConnection.query('COMMIT');
        res.status(200).json({ message: 'Registro exitoso' });

    } catch (error) {
        res.status(500).json({ message: 'Fallo en la consulta' });
        await dbConnection.query('ROLLBACK');
        
    }
});

router.get('/cursos/favoritos/:userEmail', async (req, res) => {
    const queryGetFavoritesCourses = 'SELECT * FROM courses WHERE CORRREO = ($1)';

    try {
        const userEmail = req.params.userEmail;
        
        dbConnection.query(queryGetFavoritesCourses, [userEmail], (error, response) => {
            if (error) {
                res.status(500).json({ message: 'Fallo en la consulta' });
            } else {
                console.log(response);
                res.status(200).json(response.rows);
            }
        });
        
    } catch (error) {
        res.status(500).json({ message: 'Fallo en la consulta' });        
    }
});

module.exports = router;