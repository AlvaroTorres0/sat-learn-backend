const { Router } = require('express');
const dbConnection = require('../../../db.conection');
const { v4: uuidv4 } = require('uuid');

const router = Router();


//Nuevo comentario
router.post("/cursos/comentarios/nuevo-comentario", async(req,res) => {
    const queryNewComment = `INSERT INTO COMENTARIOS_CURSOS 
    (IDCOMENTARIO,CORREO,IDCURSO,TXTCOMENTARIO,FECHACOMENTARIO) 
    VALUES ($1,$2,$3,$4,CURRENT_TIMESTAMP - interval '6 hour')`;

    try {
        const idComment = uuidv4();
        const userEmail = req.body.userEmail;
        const idCourse = req.body.idCourse;
        const txtComment = req.body.txtComment;

        const dataQueryNewComment = [idComment,userEmail,idCourse,txtComment];
        
        await dbConnection.query('BEGIN');
        await dbConnection.query(queryNewComment,dataQueryNewComment);
        await dbConnection.query('COMMIT');

        res.status(200).json({ message: 'Registro exitoso' });
        
    } catch (err) {
        res.status(500).json({ message: 'Error en el registro' });
    }
});

//Nueva respuesta a comentario
router.post("/cursos/respuestas/nueva-respuesta", async(req,res) => {
    const queryNewResponse = `INSERT INTO RESPUESTAS_COMENTARIOS 
    (IDRESPUESTA,CORREO,IDCOMENTARIO,TXTRESPUESTA,IDCURSO,FECHARESPUESTA) 
    VALUES ($1,$2,$3,$4,$5,CURRENT_TIMESTAMP - interval '6 hour')`;

    try {
        const idResponse = uuidv4();
        const userEmail = req.body.userEmail;
        const idComment = req.body.idComment;
        const txtResponse = req.body.txtResponse;
        const idCourse = req.body.idCourse;

        const dataQueryNewResponse = [idResponse,userEmail,idComment,txtResponse,idCourse];
        
        await dbConnection.query('BEGIN');

        await dbConnection.query(queryNewResponse,dataQueryNewResponse);
        await dbConnection.query('COMMIT');

        res.status(200).json({ message: 'Registro exitoso' });
        
    } catch (err) {
        console.log(err);
        res.status(500).json({ message: 'Error en el registro' });
    }
});


//* Obtener todos los comentarios de un curso
router.get("/cursos/comentarios/obtener/:idCurso", async(req,res) => {
    const queryAllComments = `SELECT IDCOMENTARIO,CORREO,TXTCOMENTARIO,FECHACOMENTARIO 
    FROM COMENTARIOS_CURSOS WHERE IDCURSO = ($1)`;

    try {
        const idCourse = [req.params.idCurso];
        const allComments = await dbConnection.query(queryAllComments, idCourse);
        res.status(200).json(allComments.rows);
    } catch (error) {
        res.status(500).json({ message: 'Error en el servidor' });
    }
});

//* Obtener todas las respuestas de los comentarios de un curso
router.get("/cursos/comentarios/respuestas/obtener/:idCurso", async(req,res) => {
    const queryAllResponses = `SELECT IDRESPUESTA,CORREO,IDCOMENTARIO,TXTRESPUESTA,IDCURSO,FECHARESPUESTA 
    FROM RESPUESTAS_COMENTARIOS WHERE IDCURSO = ($1)`;

    try {
        const idCourse = [req.params.idCurso];
        const allResponses = await dbConnection.query(queryAllResponses, idCourse);

        res.status(200).json(allResponses.rows);
    } catch (error) {
        res.status(500).json({ message: 'Error en el servidor' });
    }
});


module.exports = router;