const { Router } = require('express');
const dbConnection = require('../../db.conection');

const router = Router();

router.post('/nuevo/curso', async (req, res) => {
    const newCourseQuery = `INSERT INTO CURSOS (IDCURSO,TITULO,INSTRUCTOR,DESCRIPCION,DURACION,CAPACIDAD,MODALIDAD,FECHA,LUGARCURSO,IMGCURSO,VISIBLE,DISPONIBLE,FINALIZADO,FECHAREGISTRO)
      VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, DATE_TRUNC('second', CURRENT_TIMESTAMP - interval '6 hour'))`;
    const queryCursosArea = `INSERT INTO CURSOS_REGISTRADOS (IDADMON,IDCURSO) VALUES ($1,$2)`;

    try {
        const dataCourse = req.body.dataCourse;
        let urlImageCourse = req.body.urlImageCourse;
        
        // Validamos que la data de los atributos que pueden ser nulos sea vacía, si es así asignamos un null
        if(dataCourse.duracion === '') dataCourse.duracion = null;
        if(dataCourse.fechaHora === '') dataCourse.fechaHora = null;
        if(dataCourse.lugar === '') dataCourse.lugar = null;

        if(urlImageCourse === '') urlImageCourse = 'https://udgfvlqwkrriakrdeehq.supabase.co/storage/v1/object/public/assets/logo-sat.png';
        
        
        const administraciones = req.body.dataCourse.administraciones;

        const dataNewCourse = [
            dataCourse.idCurso,dataCourse.nombreCurso,dataCourse.correoInstructor,dataCourse.descripcion,dataCourse.duracion,dataCourse.capacidad,dataCourse.modalidad,
            dataCourse.fechaHora,dataCourse.lugar,urlImageCourse,true,true,false
        ];
        await dbConnection.query('BEGIN');
        await dbConnection.query(newCourseQuery, dataNewCourse);
        for (const arrayKey of administraciones) {
            await dbConnection.query(queryCursosArea, [arrayKey,dataCourse.idCurso]);
        }
        await dbConnection.query('COMMIT');

        res.status(200).json({ message: 'Registro exitoso' });
    } catch (e) {
        // En caso de error, realiza un rollback y maneja la excepción
        await dbConnection.query('ROLLBACK');
        console.error(e);
        res.status(500).json({ error: 'Error en el servidor' });
    }
});

router.get("/obtener/curso/:idCurso", async (req,res) => {
    const queryGetCourse = `SELECT * FROM CURSOS where IDCURSO = ($1)`;
    try {
        const idCurso = [req.params.idCurso];
        await dbConnection.query('BEGIN');
        const dataCourse = await dbConnection.query(queryGetCourse,idCurso);
        await dbConnection.query('COMMIT');
        res.status(200).json(dataCourse.rows);

    } catch(err){
        await dbConnection.query('ROLLBACK');
        res.status(500).json({ message: 'Error interno del servidor' });
    }

})

module.exports = router;
