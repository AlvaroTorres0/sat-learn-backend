const express= require('express');
const app =express();

app.use(require('./routes-auth/routes.signup'));
app.use(require('./routes-courses/routes.courses.manageCourse'));
app.use(require('./routes-courses/courses-filters/courses.filters.upcomingCourses'));
app.use(require('./routes-courses/courses-filters/courses.filters.pendingCourses'));
app.use(require('./routes-courses/courses-filters/courses.filters.popularCourses'));
app.use(require('./routes-courses/courses-filters/courses.filters.recentCourses'));
app.use(require('./routes-courses/routes.courses.subscriptions'));
app.use(require('./routes-courses/courses-surveys/routes.courses.surveys'));
app.use(require('./routes-courses/courses-comments/routes.courses.comments'));
app.use(require('./routes-searchbar/routes.searchbar'));
app.use(require('./routes-courses/courses-favorites/courses.favorites'));
app.use(require('./routes-stats/routes.stats'));
app.use(require('./routes-courses/courses-filters/courses.filters.recommendedCourses'));

module.exports = app;