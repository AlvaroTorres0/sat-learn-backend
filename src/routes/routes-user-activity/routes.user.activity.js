const { v4: uuidv4 } = require('uuid');
const dbConnection = require('../../db.conection');


const registerUserActivity = async (correo,idActividad,informacionExtra) => {
    const queryInsertActivityUser = `INSERT INTO REGISTRO_ACTIVIDAD
    (IDREGISTRO,CORREO,IDACTIVIDAD,INFORMACIONEXTRA,FECHA) 
    VALUES ($1,$2,$3,$4,CURRENT_TIMESTAMP - interval '6 hour')`;

    try {
        const idRegistro = uuidv4();
        await dbConnection.query('BEGIN');
        await dbConnection.query(queryInsertActivityUser,[idRegistro,correo,idActividad,informacionExtra]);
        await dbConnection.query('COMMIT');

    } catch (error) {
        console.log(error);
        await dbConnection.query('ROLLBACK');
    }
}


module.exports = registerUserActivity;
