require('dotenv').config({ path: './config.env' });
const express = require('express');
const routes = require('./routes/index.routes');
const cors = require('cors');
const bodyParser = require('body-parser');

const port = process.env.PORT || 3001;
const app = express();
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(routes);

app.listen(port, () =>{
    console.log('Server running on port ' + port)
})